(function ($)
{
  $(document).ready(function()
  {
    var urlQ = '',
        searchQ = '';

    if (getUrlParameter('query')) {
      urlQ = getUrlParameter('query').replace('+', ' ');
      searchQ = '#search=' + urlQ;
    }

    $('a[href$=".pdf"]').each(function(i) {
      var parser = document.createElement('a');
      parser.href = $(this).attr("href");

      var oldHref = parser.pathname;

      $(this).attr("href", "/plugins/sfPDFReaderPlugin/modules/sfPDFReaderPlugin/vendor/pdf.js/web/viewer.html?file=" + oldHref + "" + searchQ);
      $(this).attr("target", "_blank");
    });
  });
})(jQuery);