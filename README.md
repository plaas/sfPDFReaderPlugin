# PDF Reader Plugin

This project is a custom AtoM plugin. Learn more 
about AtoM at https://accesstomemory.org.

This plugin integrates Mozilla's PDF.js library into 
AtoM so all PDF links are opened in the PDF reader.

## Installation

Unzip to the atom plugins directory.

Login to atom as an administrator and enable the 
plugin from the plugins administration screen.

## Support

Author: Jonathan Wagener (Plaas)

Email: jonathan@plaas.co